import './App.css';
import StatusBar from './Components/StastusBar/StatusBar';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LoginPage from './Page/LoginPage/LoginPage';
import HomePage from './Page/HomePage/HomePage'
import DateTimePage from './Page/DateTimePage/DateTimePage'
import NetworkPage from './Page/NetworkPage/NetworkPage'
import AccountsPage from './Page/AccountsPage/AccountsPage'
import TransmissionPage from './Page/TransmissionPage/TransmissionPage'
import AboutPage from './Page/AboutPage/AboutPage'
import Ethernet from './Page/NetworkPage/Ethernet'
import Sim4G_1 from './Page/NetworkPage/Sim4G_1'
import Sim4G_2 from './Page/NetworkPage/Sim4G_2'
import DetailAcc from './Page/AccountsPage/DetailAcc'
import CreateAcc from './Page/AccountsPage/CreateAcc'
import AddFiger from './Page/AccountsPage/AddFiger'

function App() {
  return (
    <div className="w-full h-full max-w-[1920px] max-h-[1080px]  overflow-hidden box-border App bg-[#E5E7EB]">
      {/* <LoginPage /> */}
      <BrowserRouter>
        <Routes >
          <Route path='/login' element={<LoginPage />}></Route>
          <Route path='/' element={<HomePage />}></Route>
          <Route path='/datetime' element={<DateTimePage />}></Route>
          <Route path='/network' element={<NetworkPage />}></Route>
          <Route path='/accounts' element={<AccountsPage />}></Route>
          <Route path='/transmission' element={<TransmissionPage />}></Route>
          <Route path='/about' element={<AboutPage />}></Route>
          <Route path='/ethernet2' element={< Ethernet />}></Route>
          <Route path='/4Gsim1' element={<Sim4G_1 />}></Route>
          <Route path='/4Gsim2' element={<Sim4G_2 />}></Route>
          <Route path='/detail/:id' element={<DetailAcc />}></Route>
          <Route path='/create' element={<CreateAcc />}></Route>
          <Route path='/addFiger' element={<AddFiger />}></Route>
        </Routes>
      </BrowserRouter>


    </div>
  );
}

export default App;
// w-[1920px] h-[1080px]