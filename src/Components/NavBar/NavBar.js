
import React from 'react'
import NavDraw from './NavDraw'
import NavContent from './NavContent'
import StatusBar from '../StastusBar/StatusBar'

export default function NavBar() {
    return (
        <div className='h-full w-full  '>
            <StatusBar />
            <div className="flex">
                <NavDraw />
                <NavContent />

            </div>
        </div>
    )
    // max-w-[1920px] max-h-[1080px]
}
