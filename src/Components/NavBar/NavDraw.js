import React from 'react'
import { NavLink } from 'react-router-dom'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'



export default function NavDraw() {
    return (
        <div className="w-[412.5px] h-[1005px]  flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
            <div className='text-[#D1D3D4] h-[609px] '>
                <ul className='flex flex-col gap-3'>
                    <NavLink to="/" >
                        <li className=' hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                            <a className='flex items-center' href="#">
                                <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                            </a>
                        </li>
                    </NavLink>
                    <img src="../" alt="" />
                    <NavLink to="/datetime">
                        <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                            <a className='flex items-center' href="#">
                                <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                            </a>
                        </li>
                    </NavLink>
                    <NavLink to="/network">
                        <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                            <a className='flex items-center' href="#">
                                <img src={imgNet} alt="" className='inline-block ' />
                                <span className='ml-[18px] font-semibold leading-9'>Network</span>
                            </a>
                        </li>
                    </NavLink>
                    <NavLink to="/accounts">
                        <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                            <a className='flex items-center' href="#">
                                <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                            </a>
                        </li>
                    </NavLink>
                    <NavLink to="/transmission">
                        <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                            <a className='flex items-center' href="#">
                                <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                            </a>
                        </li>
                    </NavLink>
                    <NavLink to="/about">
                        <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                            <a className='flex items-center' href="#">
                                <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                <span className='ml-[18px] font-semibold leading-9'>About</span>
                            </a>
                        </li>
                    </NavLink>
                </ul>
            </div>
            <div className="flex grow items-center gap-[18px]">
                <img src="" alt="" />
                <div className="">
                    <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                    <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                </div>
            </div>
        </div>
    )
}
