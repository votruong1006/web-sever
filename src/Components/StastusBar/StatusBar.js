import React from 'react'
import imgVector from '../../assets/img/Vector.png'
import imgLan from '../../assets/img/lan.png'
import imgKey from '../../assets/img/key.png'
import imgSignal from '../../assets/img/signal.png'
import imgGroup from '../../assets/img/Group 5.png'

export default function StatusBar() {
    return (

        <div className="flex justify-between  items-center max-w-[1918.5px] max-h-[78px] w-full  leading-7 bg-[#2E6A50] px-[30px] py-[15px]">
            <p className="inline-block max-w-[322px] max-h-[42px] text-[20px] leading-7 text-white  font-semibold ">2023-08-08 12:00:00</p>
            <div className="flex max-w-[733.5px] max-h-[48px] text-white gap-[20px] text-[18px] font-medium">
                <div className="w-[48px] h-[48] flex items-center justify-center relative grow"><img src={imgVector} alt="" className='' /><img src={imgGroup} alt="" className='inline-block absolute top-0 left-0 w-full h-full' /></div>
                <div className="w-[94.5px] h-[48] relative flex items-center justify-center gap-[16.5px]"><img src={imgLan} alt="" className='inline-block' /><img src={imgGroup} alt="" className='inline-block absolute top-0 left-0 ' /><span>E1</span></div>
                <div className="w-[94.5px] h-[48] relative flex items-center justify-center gap-[16.5px]"><img src={imgLan} alt="" className='inline-block' /><img src={imgGroup} alt="" className='inline-block absolute top-0 left-0' /><span>E2</span></div>
                <div className="w-[132px] h-[48] relative flex items-center justify-between grow gap-[16.5px]"><img src={imgSignal} alt="" className='inline-block ' /><img src={imgGroup} alt="" className='inline-block   absolute top-0 left-0' /> <span>GSM 1</span></div>
                <div className="w-[132px] h-[48] relative flex items-center justify-between gap-[16.5px]"><img src={imgSignal} alt="" className='inline-block' /> <img src={imgGroup} alt="" className='inline-block absolute top-0 left-0 ' /><span>GSM 2</span></div>

                <div className=" flex items-center  "><img src={imgKey} alt="" className='' /></div>
            </div>
        </div>


    )
}
