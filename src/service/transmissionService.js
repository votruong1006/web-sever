import { https } from "./config";


export const transmissionService = {
    getTransmission: () => https.get("/transmission")
}