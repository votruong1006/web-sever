import { https } from "./config";


export const loginService = {
    postLogin: (data) => https.post(`/api/login`, data)
}