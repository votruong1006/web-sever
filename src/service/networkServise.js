import { https } from "./config";


export const networkService = {
    getNetwork: () => https.get("/network"),
    postNetwork: (value) => https.post("/network", value)
}