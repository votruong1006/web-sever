import { https } from "./config";


export const aboutService = {
    getAbout: () => https.get("/api/devinfo")
}