import { https } from "./config";

export const accService = {
    getListAcc: () => https.get("/api/list_accounts"),
    postAcc: (acc) => https.post(`/api/add_user`, acc),
    postEditAcc: (value) => https.post(`/api/edit_user`, value)
}