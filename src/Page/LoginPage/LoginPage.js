import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { loginService } from '../../service/loginService'

export default function LoginPage() {
    const [userName, setUserName] = useState("")
    const [password, setPassword] = useState("")
    const navigate = useNavigate()




    const handlePostLogin = (e) => {
        e.preventDefault()
        loginService.postLogin()
            .then((res) => {

                alert("Thêm thành công!")
                navigate("/")
                console.log(res);
            })
            .catch((err) => {
                alert("That bai!")
                console.log(err);
            });
    }
    return (
        <div className=' h-screen bg-[#0F1621] flex justify-center items-center'>
            <div className="w-[475px] h-[550px] bg-white rounded-3xl p-4">
                <div className="text-center"><span className='font-bold text-4xl ml-2 text-[#0F1621] '>WEB SEVER</span></div>
                <div className="text-center mt-5 ">
                    <p className='font-semibold text-2xl my-0'>LOGIN</p></div>
                <p className='text-center font-normal text-[14px] text-[#6C6C6C]'>Enter your credentials to access your account</p>
                <form onSubmit={handlePostLogin} className='mt-10 font-bold text-xl' action="">
                    <label className='block' htmlFor="">User name</label>
                    <input name='username' onChange={(e) => setUserName(e.target.value)} className='rounded-md text-base border-[2px] px-2 py-2 w-full outline-none mt-2' type="text" placeholder='Enter your user name' />
                    <label className='block mt-4' htmlFor="">Password</label>
                    <input name='password' onChange={(e) => setPassword(e.target.value)} className='rounded-md text-base border-[2px] px-2 py-2 w-full outline-none mt-2' type="password" placeholder='Enter your password' />
                    <button className='rounded-md my-10 border-[2px] px-2 py-2 bg-[#0F1621] w-full text-white'>SIGN IN</button>
                </form>
                <div className="text-center font-normal text-[14px]"><span>Forgot your password?</span><a className='text-[#0F1621]' href="">Reset Password</a></div>
            </div>


        </div>
    )
}
