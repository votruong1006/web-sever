import React, { useEffect, useState } from 'react'
import imgRight from '../../assets/img/right.png'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { accService } from '../../service/accountsService'
import { NavLink } from 'react-router-dom'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'
import StatusBar from '../../Components/StastusBar/StatusBar'

export default function DetailAcc() {
    const navigate = useNavigate()
    // const [acc, setAcc] = useState([])
    // useEffect(() => {
    //     fecthListAcc()
    // }, [])
    // const fecthAcc = () => {
    //     accService.getListAcc()
    //         .then((res) => {
    //             setListAcc(res.data)
    //             console.log(res);
    //         })
    //         .catch((err) => {
    //             console.log(err);
    //         });
    // }
    const [data, setData] = useState({
        username: "",
        password: "",
        role: "",
        status: "",
        fingers: []
    })
    const dataJsonn = JSON.stringify(data)
    // const fecthListAcc = () => {
    //     accService.getListAcc()
    //         .then((res) => {
    //             setData(res.data)
    //             console.log(res);
    //         })
    //         .catch((err) => {
    //             console.log(err);
    //         });
    // }
    const editAcc = (e) => {
        e.preventDefault()
        accService.postEditAcc(dataJsonn)
            .then((res) => {

                navigate("/accounts")
                alert("Thanh cong!")
            }
            )
            .catch((err) => {
                alert("That bai!")
                console.log(err);
            });
    }

    return (
        <div className="h-screen">
            <StatusBar />
            <div className="flex h-full">
                <div className="w-[412.5px]  flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
                    <div className='text-[#D1D3D4] h-[609px] '>
                        <ul className='flex flex-col gap-3'>
                            <NavLink to="/" >
                                <li className=' hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                                    </a>
                                </li>
                            </NavLink>

                            <NavLink to="/datetime">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/network">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgNet} alt="" className='inline-block ' />
                                        <span className='ml-[18px] font-semibold leading-9'>Network</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/accounts">
                                <li className='bg-slate-700 rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/transmission">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/about">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>About</span>
                                    </a>
                                </li>
                            </NavLink>
                        </ul>
                    </div>
                    {/* <div className="flex grow items-center gap-[18px]">
                        <img src="" alt="" />
                        <div className="">
                            <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                            <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                        </div>
                    </div> */}
                </div>
                <div className=" max-w-[1506px] max-h-[1005px] w-full h-full ">
                    <div className=' flex items-center justify-between py-3 px-6  gap-[90px] bg-[#F4F4F5] border-[1.5px] border-solid '>
                        <div className="flex items-center gap-[6px]">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Accounts</div>
                            <img src={imgRight} alt="" />
                            <div className="text-[27px] font-medium text-[#3F3F46]">update</div>
                        </div>
                        <div className="flex items-center justify-end gap-[18px]">
                            <Link to="/accounts"><button className='py-3 px-5 bg-[#E4E4E7]  rounded-[6px]  text-[19px] font-semibold'>Cancel</button></Link>
                            <button onClick={editAcc} className='py-3 px-6 bg-[#60A5FA]  rounded-[6px]  text-[19px] font-semibold '>Save</button>
                        </div>
                    </div>
                    <div className="flex gap-14 mt-10 ml-5">
                        <div className="flex flex-col items-start gap-[57px] max-w-[561px] w-1/2">
                            <div className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium">
                                <label className=' '>User name</label>
                                <input type="text" name='username' value={data.username} onChange={e => setData({ ...data, username: e.target.value })} className='text-[#71717A] outline-none w-full py-3 px-6 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />
                            </div>
                            <div className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium">
                                <label className=' '>Reset password</label>
                                <input type="password" name='password' onChange={e => setData({ ...data, password: e.target.value })} placeholder='Enter new password' className='text-[#71717A] outline-none w-full py-3 px-6 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />
                                <label className=' text-gray-500 font-normal text-[37.656px] leading-[56px]'>Enter new password to reset</label>
                            </div>
                        </div>
                        <div className="flex flex-col items-start  gap-[57px] max-w-[561px] w-1/2">
                            <div className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium w-full">
                                <label className=''>Role</label>
                                <select value={data.role} name='role' onChange={e => setData({ ...data, role: e.target.value })} className='outline-none w-full py-3 px-6 bg-[#E4E4E7] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]'>

                                    <option defaultChecked className='' value="admin">ADMIN</option>
                                    <option className='' value="operator">OPERATOR</option>
                                    <option className='' value="viewer">USER</option>
                                </select>
                            </div>
                            <div className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium w-full">
                                <label className=''>Status</label>
                                <select value={data.status} onChange={e => setData({ ...data, status: e.target.value })} className='outline-none w-full py-3 px-6 bg-[#E4E4E7] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]'>

                                    <option defaultChecked value="acctive">ACTIVE</option>
                                    <option value="deacctive">DEACTIVE</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="mt-16  ml-5">
                        <p className='text-[#27272A] text-[37.5px] font-medium' htmlFor="">Fingerprint(0/5)</p>
                        <div className="flex mt-5 gap-5">
                            <div className="w-[128.465px]">Figer1</div>
                            <div className="w-[128.465px]">Figer2</div>
                            <Link to="/addFiger"><div className="w-[128.465px] text-[37.5px] font-bold h-[128.465px] cursor-pointer flex justify-center items-center p-4 rounded-full bg-gray-300"><p>+</p></div></Link>
                            <div className="w-[128.465px] text-[37.5px] font-bold h-[128.465px] cursor-pointer flex justify-center items-center p-4 rounded-full bg-gray-300"><p>-</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
