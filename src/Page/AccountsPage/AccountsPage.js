import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import imgRight from '../../assets/img/right.png'
import { accService } from '../../service/accountsService'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'
import StatusBar from '../../Components/StastusBar/StatusBar'

export default function AccountsPage() {
    const [listAcc, setListAcc] = useState([])
    useEffect(() => {
        fecthListAcc()
    }, [])
    const fecthListAcc = () => {
        accService.getListAcc()
            .then((res) => {
                setListAcc(res.data)
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });
    }


    const renderTBody = () => {
        return listAcc?.map((acc, index) => {
            return <tr className=" overflow-y-scroll ">
                <td className="p-6">
                    1
                </td>
                <td className="p-6">
                    {acc.username}
                </td>
                <td className="p-6">
                    <span className="bg-[#D5EEFA] rounded-[6px] px-3 py-[6px]"> {acc.role}</span>
                </td>
                <td className="p-6 ">
                    <span className="bg-[#10B981] rounded-[6px] py-[3px] px-[15px]">{acc.status}</span>
                </td>
                <td className="p-6">
                    0/5
                </td>
                <td className="p-6 text-right">
                    <NavLink to={`/detail/${acc.id}`}><button className="font-medium text-black py-3 px-5 rounded-[6px] bg-blue-400">Edit</button></NavLink>
                </td>
            </tr>
        })
    }
    return (
        <div className="h-screen">
            <StatusBar />
            <div className="flex h-full">
                <div className="w-[412.5px]   flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
                    <div className='text-[#D1D3D4] h-full '>
                        <ul className='flex flex-col gap-3'>
                            <NavLink to="/" >
                                <li className=' hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                                    </a>
                                </li>
                            </NavLink>

                            <NavLink to="/datetime">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/network">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgNet} alt="" className='inline-block ' />
                                        <span className='ml-[18px] font-semibold leading-9'>Network</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/accounts">
                                <li className='bg-slate-700 rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/transmission">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/about">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>About</span>
                                    </a>
                                </li>
                            </NavLink>
                        </ul>
                    </div>
                    {/* <div className="flex grow items-center gap-[18px]">
                        <img src="" alt="" />
                        <div className="">
                            <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                            <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                        </div>
                    </div> */}
                </div>
                <div className='max-w-[1507.5px] max-h-[563.106px] w-full h-full'>
                    <div className='max-w-[1506px] w-full flex items-center justify-between py-3 px-6  gap-[90px] bg-[#F4F4F5] border-[1.5px] border-solid '>
                        <div className="flex items-center gap-[6px]">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Accounts</div>
                            <img src={imgRight} alt="" />
                            <div className="text-[27px] font-medium text-[#3F3F46]">All Accounts</div>
                        </div>
                        <div className="">
                            <NavLink to="/create"><button className='py-3 px-5 bg-[#60A5FA]  rounded-[6px] text-[#3F3F46] text-[19px] font-semibold'>Create accounts</button></NavLink>

                        </div>
                    </div>
                    <div className=" overflow-x-auto overflow-scroll p-3">
                        <table className="w-full text-sm text-left overflow-y-scroll text-gray-500 max-h-[727.5px]">
                            <thead className="text-lg font-semibold  text-gray-500 bg-white">
                                <tr>
                                    <th scope="col" className="p-6">
                                        #
                                    </th>
                                    <th scope="col" className="p-6 ">
                                        USER NAME
                                    </th>
                                    <th scope="col" className="p-6  ">
                                        ROLE
                                    </th>
                                    <th scope="col" className="p-6  ">
                                        STATUS
                                    </th>
                                    <th scope="col" className="p-6 ">
                                        NO. FINGERPRINT
                                    </th>
                                    <th scope="col" className="p-6  text-right">
                                        <span className="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="font-medium text-2xl text-gray-900 overflow-y-scroll">
                                {renderTBody()}

                            </tbody>
                        </table>
                        {/* <div className="flex items-center justify-between gap-[15px] px-6 py-0 max-w-[1447.5px] max-h-[99px]">
                    <div className="text-[21px] font-medium text-gray-900">Showing 8-40</div>
                    <nav aria-label="Page navigation example">
                        <ul className="flex items-center -space-x-px h-8 text-sm">
                            <li>
                                <a href="#" className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                    <span className="sr-only">Previous</span>
                                    <svg className="w-2.5 h-2.5 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 1 1 5l4 4" />
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">1</a>
                            </li>
                            <li>
                                <a href="#" className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">2</a>
                            </li>
                            <li>
                                <a href="#" aria-current="page" className="z-10 flex items-center justify-center px-3 h-8 leading-tight text-blue-600 border border-blue-300 bg-blue-50 hover:bg-blue-100 hover:text-blue-700 dark:border-gray-700 dark:bg-gray-700 dark:text-white">3</a>
                            </li>
                            <li>
                                <a href="#" className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">...</a>
                            </li>
                            <li>
                                <a href="#" className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">10</a>
                            </li>
                            <li>
                                <a href="#" className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">
                                    <span className="sr-only">Next</span>
                                    <svg className="w-2.5 h-2.5 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="m1 9 4-4-4-4" />
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div> */}
                    </div>

                </div>
            </div>
        </div>
    )
}
