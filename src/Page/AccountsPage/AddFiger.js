import React from 'react'
import imgRight from '../../assets/img/right.png'
import { Link } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'
import StatusBar from '../../Components/StastusBar/StatusBar'
export default function AddFiger() {
    return (
        <div className="">
            <StatusBar />
            <div className="flex">
                <div className="w-[412.5px] h-[1005px]  flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
                    <div className='text-[#D1D3D4] h-[609px] '>
                        <ul className='flex flex-col gap-3'>
                            <NavLink to="/" >
                                <li className=' hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                                    </a>
                                </li>
                            </NavLink>

                            <NavLink to="/datetime">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/network">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgNet} alt="" className='inline-block ' />
                                        <span className='ml-[18px] font-semibold leading-9'>Network</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/accounts">
                                <li className='bg-slate-700 rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/transmission">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/about">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>About</span>
                                    </a>
                                </li>
                            </NavLink>
                        </ul>
                    </div>
                    {/* <div className="flex grow items-center gap-[18px]">
                        <img src="" alt="" />
                        <div className="">
                            <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                            <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                        </div>
                    </div> */}
                </div>
                <div className='max-w-[1506px] max-h-[1005px] w-full h-full'>
                    <div className=' flex items-center justify-between py-3 px-6  gap-[90px] bg-[#F4F4F5] border-[1.5px] border-solid '>
                        <div className="flex items-center gap-[6px]">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Accounts</div>
                            <img src={imgRight} alt="" />
                            <div className="text-[27px] font-medium text-[#3F3F46]">Add Fingerprinter </div>
                        </div>
                        <div className="flex items-center justify-end gap-[18px]">
                            <Link to="/detail/:id"><button className='py-3 px-5 bg-[#E4E4E7]  rounded-[6px] text-[#3F3F46]  text-[19px] font-semibold'>Cancel</button></Link>
                            <button className='py-3 px-6 bg-[#60A5FA]  rounded-[6px] text-[#3F3F46] text-[19px] font-semibold '>Save</button>
                        </div>
                    </div>
                    <div className="text-center text-[#3F3F46] flex flex-col items-center mt-16 p-3">
                        <p className=' text-[35.5px] font-medium'>Touch and hold your finger 3 seconds, repeat 3 times</p>
                        <div className="mt-8 w-[128.465px] text-[37.5px] font-bold h-[128.465px] bg-gray-300 rounded-full "></div>
                        <p className='mt-8 text-2xl'>Progress 0/3</p>
                        <p className='mt-8 text-[35.5px] bg-white py-1 px-2 rounded'>Finger1</p>
                        <p className='mt-4 font-bold text-gray-500'>Set name for fingerprint ater recognized</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
