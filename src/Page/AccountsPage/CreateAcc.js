import React, { useState } from 'react'
import imgRight from '../../assets/img/right.png'
import { accService } from '../../service/accountsService'
import { Link, useNavigate } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'
import StatusBar from '../../Components/StastusBar/StatusBar'

export default function CreateAcc() {
    const [data, setData] = useState({
        username: "",
        password: "",
        role: "ADMIN",
        status: "ACTIVE",
        fingers: []
    })
    const navigate = useNavigate()
    // const dataJson = JSON.stringify(data)



    const handleAdd = (e) => {
        e.preventDefault()
        accService.postAcc(data)
            .then((res) => {

                alert("Thêm thành công!")
                navigate("/accounts")
                console.log(res);
            })
            .catch((err) => {
                alert("That bai!")
                console.log(err);
            });
    }
    return (
        <div className="h-screen">
            <StatusBar />
            <div className="flex h-screen">
                <div className="w-[412.5px]  h-screen flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
                    <div className='text-[#D1D3D4] h-[609px] '>
                        <ul className='flex flex-col gap-3'>
                            <NavLink to="/" >
                                <li className=' hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                                    </a>
                                </li>
                            </NavLink>

                            <NavLink to="/datetime">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/network">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgNet} alt="" className='inline-block ' />
                                        <span className='ml-[18px] font-semibold leading-9'>Network</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/accounts">
                                <li className='bg-slate-700 rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/transmission">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/about">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>About</span>
                                    </a>
                                </li>
                            </NavLink>
                        </ul>
                    </div>
                    {/* <div className="flex grow items-center gap-[18px]">
                        <img src="" alt="" />
                        <div className="">
                            <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                            <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                        </div>
                    </div> */}
                </div>
                <div className=" max-w-[1506px] max-h-[1005px] w-full h-full ">
                    <div className=' flex items-center justify-between py-3 px-6  gap-[90px] bg-[#F4F4F5] border-[1.5px] border-solid '>
                        <div className="flex items-center gap-[6px]">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Accounts</div>
                            <img src={imgRight} alt="" />
                            <div className="text-[27px] font-medium text-[#3F3F46]">Create</div>
                        </div>
                        <div className="flex items-center justify-end gap-[18px]">
                            <Link to="/accounts"><button className='py-3 px-5 bg-[#E4E4E7]  rounded-[6px]  text-[19px] font-semibold'>Cancel</button></Link>
                            <button onClick={handleAdd} className='py-3 px-6 bg-[#60A5FA]  rounded-[6px] text-[19px] font-semibold '>Save</button>
                        </div>
                    </div>
                    <div className="flex gap-14 mt-10 ml-5">
                        <div className="flex flex-col items-start gap-[57px] max-w-[561px] w-1/2">
                            <form className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium">
                                <label className=' '>User name</label>
                                <input name='username' onChange={e => setData({ ...data, username: e.target.value })} type="text" placeholder='Enter user name' className='text-[#71717A] outline-none w-full py-3 px-6 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />
                            </form>
                            <form className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium">
                                <label className=' '>Password</label>
                                <input name='password' onChange={e => setData({ ...data, password: e.target.value })} type="password" placeholder='Enter password' className='text-[#71717A] outline-none w-full py-3 px-6 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />
                            </form>
                        </div>
                        <div className="flex flex-col items-start  gap-[57px] max-w-[561px] w-1/2">
                            <form className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium w-full">
                                <label className=''>Role</label>
                                <select defaultValue="ADMIN" name='role' onChange={e => setData({ ...data, role: e.target.value })} className='outline-none w-full py-3 px-6 bg-[#E4E4E7] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]'>

                                    <option className='' value="ADMIN">ADMIN</option>
                                    <option className='' value="OPERATOR">OPERATOR</option>
                                    <option className='' value="VIEWER">VIEWER</option>
                                </select>
                            </form>
                            <form className="flex flex-col items-start gap-3 text-[#27272A] text-[37.5px] font-medium w-full">
                                <label name='status' className=''>Status</label>
                                <select defaultValue="ACTIVE" onChange={e => setData({ ...data, status: e.target.value })} className='outline-none w-full py-3 px-6 bg-[#E4E4E7] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]'>

                                    <option value="ACTIVE">ACTIVE</option>

                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
