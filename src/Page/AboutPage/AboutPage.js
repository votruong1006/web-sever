import React, { useEffect, useState } from 'react'
import { aboutService } from '../../service/aboutService';
import { NavLink } from 'react-router-dom'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'
import StatusBar from '../../Components/StastusBar/StatusBar';
export default function AboutPage() {
    const [about, setAbout] = useState({})

    const fetchAbout = () => {
        aboutService.getAbout()
            .then((res) => {
                setAbout(res.data)
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });
    }
    useEffect(() => { fetchAbout() }, [])
    return (
        <div className="h-screen">
            <StatusBar />
            <div className="flex">
                <div className="w-[412.5px] h-[1005px]  flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
                    <div className='text-[#D1D3D4] h-[609px] '>
                        <ul className='flex flex-col gap-3'>
                            <NavLink to="/" >
                                <li className=' hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                                    </a>
                                </li>
                            </NavLink>

                            <NavLink to="/datetime">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/network">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgNet} alt="" className='inline-block ' />
                                        <span className='ml-[18px] font-semibold leading-9'>Network</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/accounts">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/transmission">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/about">
                                <li className='bg-slate-700 rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>About</span>
                                    </a>
                                </li>
                            </NavLink>
                        </ul>
                    </div>
                    {/* <div className="flex grow items-center gap-[18px]">
                        <img src="" alt="" />
                        <div className="">
                            <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                            <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                        </div>
                    </div> */}
                </div>
                <div className='max-w-[1507.5px] max-h-[563.106px] w-full h-full '>
                    <div className=' flex items-center justify-between py-[18px] px-[24px]  gap-[90px] bg-[#F4F4F5] border-[1.5px] border-solid '>
                        <div className="flex items-center gap-[6px]">
                            <div className="text-[27px] font-medium text-[#3B82F6]">About</div>
                        </div>
                        <div className="">
                        </div>
                    </div>
                    <div className="flex flex-col items-start max-w-[1495.5px] gap-3  ml-3">
                        <div className="max-w-[1495.488px] w-full flex items-center gap-[6px] py-[18px] px-[24px] bg-gray-100 border-b-[1.5px] border-b-gray-100">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Order code</div>
                            <i className="fa fa-angle-right text-[#A1A1AA] " ></i>
                            <div className="text-[27px] font-medium text-[#3F3F46]">{about.ordercode}</div>
                        </div>
                        <div className="max-w-[1495.488px] w-full flex items-center gap-[6px] py-[18px] px-[24px] bg-gray-100 border-b-[1.5px] border-b-gray-100">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Serial number</div>
                            <i className="fa fa-angle-right text-[#A1A1AA] " ></i>
                            <div className="text-[27px] font-medium text-[#3F3F46]">{about.serialnum}</div>
                        </div>
                        <div className="max-w-[1495.488px] w-full flex items-center gap-[6px] py-[18px] px-[24px] bg-gray-100 border-b-[1.5px] border-b-gray-100">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Software  version</div>
                            <i className="fa fa-angle-right text-[#A1A1AA] " ></i>
                            <div className="text-[27px] font-medium text-[#3F3F46]">{about.swversion}</div>
                        </div>
                        <div className='max-w-[1495.488px] w-full flex items-center justify-between py-[18px] px-[24px]  gap-[90px] bg-[#F4F4F5] border-[1.5px] border-solid '>
                            <div className="flex items-center gap-[6px]">
                                <div className="text-[27px] font-medium text-[#3B82F6]">Reboot</div>
                                <i className="fa fa-angle-right text-[#A1A1AA] " ></i>
                                <div className="text-[27px] font-medium text-[#3F3F46]">Reboot through current settings</div>
                            </div>
                            <div className="">
                                <button className='py-[15px] px-6 bg-gray-200 rounded-[6px] text-[#3F3F46] text-[19px] font-semibold'>Reboot</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
