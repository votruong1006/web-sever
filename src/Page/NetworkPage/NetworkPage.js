import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import imgRight from '../../assets/img/right.png'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'
import StatusBar from '../../Components/StastusBar/StatusBar'

export default function NetworkPage() {
    const [value, setValue] = useState(

    )
    return (
        <div className="h-screen">
            <StatusBar />
            <div className="flex">
                <div className="w-[412.5px] h-[1005px]  flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
                    <div className='text-[#D1D3D4] h-[609px] '>
                        <ul className='flex flex-col gap-3'>
                            <NavLink to="/" >
                                <li className=' hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                                    </a>
                                </li>
                            </NavLink>

                            <NavLink to="/datetime">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/network">
                                <li className='bg-slate-700 rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgNet} alt="" className='inline-block ' />
                                        <span className='ml-[18px] font-semibold leading-9'>Network</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/accounts">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/transmission">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/about">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>About</span>
                                    </a>
                                </li>
                            </NavLink>
                        </ul>
                    </div>
                    {/* <div className="flex grow items-center gap-[18px]">
                        <img src="" alt="" />
                        <div className="">
                            <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                            <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                        </div>
                    </div> */}
                </div>
                <div className=" max-w-[1506px] max-h-[1005px] w-full h-full ">
                    <div className=' flex items-center justify-between py-3 px-6  gap-[90px] bg-[#F4F4F5] border-[1.5px] border-solid '>
                        <div className="flex items-center gap-[6px]">
                            <div className="text-[27px] font-medium text-[#3B82F6]">Network</div>
                            <img src={imgRight} alt="" />
                            <div className="text-[27px] font-medium text-[#3F3F46]">Information</div>
                        </div>
                        <div className="flex items-center justify-end gap-[18px]">
                            <button className='py-3 px-5 bg-[#E4E4E7]  rounded-[6px] text-[#3F3F46]  text-[19px] font-semibold'>Cancel</button>
                            <button className='py-3 px-6 bg-[#60A5FA]  rounded-[6px] text-[#3F3F46] text-[19px] font-semibold '>Save</button>
                        </div>
                    </div>

                    <div className="max-w-[934.5px] flex justify-around items-start p-2  rounded-xl text-gray-600 border-solid border-[1.5px] border-gray-200 bg-gray-100 ml-14 mt-4">
                        <NavLink to="/network"><a className='py-1 px-3 font-medium text-center text-[37.5px] rounded-[6px] bg-gray-50 text-blue-600 border-[2px] border-blue-600'>Ethernet 1 </a></NavLink>
                        <NavLink to="/ethernet2"><a className='py-1 px-3 font-medium text-center text-[37.5px] rounded-[6px] bg-gray-50 '>Ethernet 2 </a></NavLink>
                        <NavLink to="/4Gsim1"><a className='py-1 px-3 font-medium text-center text-[37.5px] rounded-[6px] bg-gray-50'>4G SIM 1</a></NavLink>
                        <NavLink to="/4Gsim2"><a className='py-1 px-3 font-medium text-center text-[37.5px] rounded-[6px] bg-gray-50'>4G SIM 2 </a></NavLink>
                    </div>


                    <div className="flex flex-col items-start gap-4  max-w-[1432.5px]  ml-16 mt-5">
                        <div className="flex items-center max-w-[1089px] gap-[18px] text-[30px] ">
                            <label className='inline-block w-[217.5px]  text-center font-medium text-gray-800'>DHCP Auto</label>
                            <label className="relative outline-none inline-flex items-center cursor-pointer ">
                                <input type="checkbox" defaultValue className="sr-only outline-none peer" />
                                <div className="w-16 h-8 bg-[#FAFAFA] rounded-full peer peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-500 after:border-[2px] after:rounded-full after:h-7 after:w-7 after:outline-none after:transition-all dark:border-gray-600 peer-checked:bg-blue-500 outline-none" />
                            </label>
                        </div>
                        <div className="flex items-center max-w-[1089px] gap-[18px] text-[30px]">
                            <label className='inline-block w-[217.5px]  text-right font-medium text-gray-800 '>IP Address</label>
                            <input type="text" placeholder='0.0.0.0' className='text-[#71717A] font-normal outline-none w-[853.5px]  py-2 px-3 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />

                        </div>
                        <div className="flex items-center max-w-[1089px] gap-[18px] text-[30px] ">
                            <label className='inline-block w-[217.5px]  text-right font-medium text-gray-800  '>Subnet Mask</label>
                            <input type="text" placeholder='0.0.0.0' className='text-[#71717A] font-normal outline-none w-[853.5px]  py-2 px-3 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />

                        </div>
                        <div className="flex items-center max-w-[1089px] gap-[18px] text-[30px] ">
                            <label className='inline-block w-[217.5px]  text-right font-medium text-gray-800  '>Gateway</label>
                            <input type="text" placeholder='0.0.0.0' className='text-[#71717A] font-normal outline-none w-[853.5px]  py-2 px-3 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />

                        </div>
                        <div className="flex items-center max-w-[1089px] gap-[18px] text-[30px] ">
                            <label className='inline-block w-[217.5px]  text-right font-medium text-gray-800  '>DNS</label>
                            <input type="text" placeholder='0.0.0.0' className='text-[#71717A] font-normal outline-none w-[853.5px]  py-2 px-3 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />

                        </div>
                        <div className="flex items-center max-w-[1089px] gap-[18px] text-[30px] ">
                            <label className='inline-block w-[217.5px]  text-right font-medium text-gray-800  '>MAC Address</label>
                            <input type="text" placeholder='0.0.0.0' className='text-[#71717A] font-normal outline-none w-[853.5px]  py-2 px-3 bg-[#FAFAFA] rounded-[6px] border-solid border-[1.5px] border-[#D4D4D8]' />

                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}
