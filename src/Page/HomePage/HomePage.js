import React, { useEffect, useState } from 'react'
import imgRight from '../../assets/img/right.png'
import { homeServise } from '../../service/homeService'
import { NavLink } from 'react-router-dom'
import imgHome from '../../assets/img/home.png'
import imgDate from '../../assets/img/date.png'
import imgNet from '../../assets/img/network.png'
import imgAcc from '../../assets/img/user-circle.png'
import imgLink from '../../assets/img/share.png'
import imgAbout from '../../assets/img/about.png'
import StatusBar from '../../Components/StastusBar/StatusBar'

export default function HomePage() {

    const [infoHome, setInfoHome] = useState({})


    const fecthInfoHome = () => {
        homeServise.getInfoHome()
            .then((res) => {
                setInfoHome(res.data)

            })
            .catch((err) => {
                console.log(err);
            });
    }
    useEffect(() => { fecthInfoHome() }, [])
    return (
        <div className="w-full h-screen overflow-hidden">
            <StatusBar />
            <div className="flex h-screen w-full ">
                <div className="w-[412.5px]   flex flex-col items-start gap-[211.5px]  text-white py-9 px-[18px]  bg-[#0F1621] box-border">
                    <div className='text-[#D1D3D4] h-full '>
                        <ul className='flex flex-col gap-3'>
                            <NavLink to="/" >
                                <li className=' bg-slate-700 rounded-[18px] w-[376.5px] text-[37.5px] h-[72px] flex items-center p-[18px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgHome} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] leading-9 font-semibold'>Home</span>
                                    </a>
                                </li>
                            </NavLink>

                            <NavLink to="/datetime">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgDate} alt="" className='inline-block  w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Date Time</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/network">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px]'>
                                    <a className='flex items-center' href="#">
                                        <img src={imgNet} alt="" className='inline-block ' />
                                        <span className='ml-[18px] font-semibold leading-9'>Network</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/accounts">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAcc} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Accounts</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/transmission">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgLink} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>Transmission</span>
                                    </a>
                                </li>
                            </NavLink>
                            <NavLink to="/about">
                                <li className='hover:bg-slate-700 hover:rounded-[18px] w-[376.5px] h-[72px] flex items-center p-[18px] text-[37.5px] '>
                                    <a className='flex items-center' href="#">
                                        <img src={imgAbout} alt="" className='inline-block w-9 h-9' />
                                        <span className='ml-[18px] font-semibold leading-9'>About</span>
                                    </a>
                                </li>
                            </NavLink>
                        </ul>
                    </div>
                    {/* <div className="flex grow items-center gap-[18px]">
                        <img src="" alt="" />
                        <div className="">
                            <span className='text-[22.5px] font-semibold'>Dinhngo</span>
                            <p className='text-[18px] font-medium'>Dinh.Ngo@vietan-service.com</p>
                        </div>
                    </div> */}
                </div>
                <div className="max-w-[1506px] w-full h-screen">
                    <div className="flex items-center gap-[6px] py-5 px-6 bg-[#F4F4F5] border-[1.5px] border-solid">
                        <div className="text-[27px] font-medium text-[#3B82F6]">Home</div>
                        <img src={imgRight} alt="" />
                        <div className="text-[27px] font-medium text-[#3F3F46]">Device Informations</div>
                    </div>
                    <div className="text-[27px] font-medium text-[#3F3F46] py-[18px] px-[24px] h-full">
                        <div className="flex items-center mb-4">
                            <p className='w-80'>Model</p>
                            <p className='grow uppercase'>ENVIDATA</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <p className='w-80'>Manufacture</p>
                            <p className='grow uppercase'>VIET AN GROUP JOIN STOCK COMPANY (<a className='text-blue-500 lowercase' href="https://mockapi.io/projects/63bea800f5cfc0949b5d49cb">https://mockapi.io/projects</a>) </p>
                        </div>
                        <div className="flex items-center mb-4 box-border">
                            <p className='w-80'>CPU</p>
                            <p className='grow box-border'>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem incidunt illum quis quod laboriosam lo</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <p className='w-80'>Memory</p>
                            <p className='grow'>Lorem ipsum dolor sit amet consectetur</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <p className='w-80'>4G LTE</p>
                            <p className='grow'>Lorem ipsum dolor sit amet consectetur, adipisicing elit</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <p className='w-80'>Operation System</p>
                            <p className='grow'>Yocto linux</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <p className='w-80'>Hardware Version</p>
                            <p className='grow'>{infoHome.hwversion}</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <p className='w-80'>Software Version</p>
                            <p className='grow'>{infoHome.swversion}</p>
                        </div>
                        <div className="flex items-center mb-4">
                            <p className='w-80'>Sevice Uptime</p>
                            <p className='grow'>{infoHome.uptime}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
